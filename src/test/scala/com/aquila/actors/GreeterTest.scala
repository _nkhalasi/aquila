package com.aquila.actors

import org.scalatest.FunSuite
import akka.actor.ActorSystem
import akka.actor.Props
import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner

@RunWith(classOf[JUnitRunner])
class GreeterTestSuite extends FunSuite {
  test("Greet a man") {
    val system = ActorSystem("GreeterSystem")
    val greeter = system.actorOf(Props[Greeter], "Greeter")
    greeter ! Man
    greeter ! Woman
    assert (1 == 1)
  }
}