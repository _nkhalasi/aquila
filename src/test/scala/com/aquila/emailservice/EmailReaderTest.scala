package com.aquila.emailservice

import org.scalatest.FunSuite
import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner

@RunWith(classOf[JUnitRunner])
class EmailReaderTestSuite extends FunSuite {
//    test("Read all mails") {
//    	readEmails from 
//			EmailProvider(host="imap.gmail.com", port=993, username="abc@gmail.com", password="xxxxx")
//    	assert(1 == 1)
//    }
    test("Fetch recent mails") {
    	fetchEmails from 
			EmailProvider(host="imap.gmail.com", port=993, username="abc@gmail.com", password="xxxxx")
    	assert(1 == 1)
    }
}