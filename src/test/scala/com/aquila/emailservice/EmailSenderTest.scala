package com.aquila.emailservice

import org.scalatest.FunSuite
import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner

@RunWith(classOf[JUnitRunner])
class EmailSenderTestSuite extends FunSuite {
  test("Send text mail") {
    send a new Mail(
      from = ("abc@gmail.com", "Naresh Khalasi"),
      to = List("def@gmail.com"),
      subject = "Test Mail",
      message = "Test mail contents",
      session = MailSession(username = "abc@gmail.com", password = "xxxxxx",
        smtpAuth = new java.lang.Boolean("true"), enableTls = new java.lang.Boolean("true"),
        smtpHost = "smtp.gmail.com", smtpPort = 587))
    assert(1 == 1)
  }

  test("Send rich text mail") {
    send a new Mail(
      from = ("abc@gmail.com", "Naresh Khalasi"),
      to = List("def@gmail.com"),
      subject = "Test Mail", message = "",
      richMessage = Option("<h1>Test mail contents</h1>"),
      session = MailSession(username = "abc@gmail.com", password = "xxxxxx",
        smtpAuth = new java.lang.Boolean("true"), enableTls = new java.lang.Boolean("true"),
        smtpHost = "smtp.gmail.com", smtpPort = 587))
    assert(1 == 1)
  }
}