package com.aquila.emailservice

import javax.mail.PasswordAuthentication
import javax.mail.Authenticator

object EmailAuthenticator {
  def apply(username: String, password: String) = new Authenticator() {
    override def getPasswordAuthentication = new PasswordAuthentication(username, password)
  }
}