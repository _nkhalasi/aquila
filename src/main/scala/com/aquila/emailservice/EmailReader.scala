package com.aquila.emailservice

import java.util.Properties
import javax.mail.Session
import javax.mail.Store
import javax.mail.Folder
import javax.mail.Flags
import javax.mail.Flags.Flag
import javax.mail.search.AndTerm
import javax.mail.search.FlagTerm
import javax.mail.search.ComparisonTerm
import javax.mail.search.ReceivedDateTerm
import javax.mail.NoSuchProviderException
import javax.mail.MessagingException
import javax.mail.FetchProfile
import org.joda.time.DateTime
import org.joda.time.Period


object EmailProvider {
  def apply(host:String, port:Integer, username:String, password:String) = {
    val config = Map("mail.store.protocol" -> "imaps", 
        "mail.host" -> host,
        "mail.imap.host" -> host, "mail.imap.port" -> port,
        "mail.debug" -> true)
    val props = new Properties()
    config.foreach(x => props.setProperty(x._1, x._2.toString()))
    Session.getInstance(props, EmailAuthenticator(username, password))
  }
}

class ImapEmailReader(session:Session) {
  def getEmails = {
    val store = session.getStore("imaps")
    try {
    	store.connect()
    	val inbox = store.getFolder("Inbox")
    	inbox.open(Folder.READ_ONLY)
      
    	val messages = inbox.search(new FlagTerm(new Flags(Flag.SEEN), false))
    	for (message <- messages) {
			println(message.getSubject())
    	}
    	inbox.close(true)
    } catch {
      case e: NoSuchProviderException =>  e.printStackTrace()
                                          System.exit(1)
      case me: MessagingException =>      me.printStackTrace()
                                          System.exit(2)
    } finally {
      store.close()
    }
  }
  
  def fetchEmails = {
    val store = session.getStore("imaps")
    try {
      store.connect()
      val inbox = store.getFolder("Inbox")
      inbox.open(Folder.READ_ONLY)
      val _3daysAgo = new DateTime().plus(Period.days(-3)).toDate()
      
      val unread = new FlagTerm(new Flags(Flag.SEEN), false)
      val newerThan = new ReceivedDateTerm(ComparisonTerm.GT, _3daysAgo)
      val criteria = new AndTerm(unread, newerThan)
      val messages = inbox.search(criteria)
      val fetchProfile = new FetchProfile()
      fetchProfile.add(FetchProfile.Item.ENVELOPE)
      fetchProfile.add(FetchProfile.Item.FLAGS)
      fetchProfile.add("X-mailer")
      inbox.fetch(messages, fetchProfile)
      for (message <- messages) {
        println(message.getSubject())
      }
      inbox.close(true)
    } catch {
      case e: NoSuchProviderException => e.printStackTrace()
    		  							System.exit(1)
      case me: MessagingException =>  me.printStackTrace()
      									System.exit(2)
    } finally {
      store.close()
    }
  }
}

object readEmails {
  def from(session: Session) = {
    new ImapEmailReader(session) getEmails
  }
}

object fetchEmails {
  def from(session: Session) = {
    new ImapEmailReader(session) fetchEmails
  }
}