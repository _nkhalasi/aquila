package com.aquila.emailservice

import javax.mail.Session
import javax.mail.internet.MimeMessage
import javax.mail.Message
import javax.mail.internet.InternetAddress
import javax.mail.Transport
import java.util.Properties

sealed abstract class MailType
case object Plain extends MailType
case object Rich extends MailType
case object MultiPart extends MailType

object EmailAddress {
  def apply(address: String) = new InternetAddress(address)
}

object MailSession {
  def apply(username: String, password: String,
    smtpAuth: Boolean, enableTls: Boolean,
    smtpHost: String, smtpPort: Integer,
    debug: Boolean = new java.lang.Boolean("false")) = {
    val props = new Properties()
    props.put("mail.debug", debug.toString())
    props.put("mail.smtp.auth", smtpAuth.toString())
    props.put("mail.smtp.starttls.enable", enableTls.toString())
    props.put("mail.smtp.host", smtpHost)
    props.put("mail.smtp.port", smtpPort)
    Session.getInstance(props, EmailAuthenticator(username, password))
  }
}

case class Mail(
  from: (String, String), // (email -> name)
  to: Seq[String],
  cc: Seq[String] = Seq.empty,
  bcc: Seq[String] = Seq.empty,
  subject: String,
  message: String,
  richMessage: Option[String] = None,
  attachment: Option[(java.io.File)] = None,
  session: Session) {

  val emailMessage = new MimeMessage(session)
  emailMessage.setSubject(subject)
  if (richMessage.isDefined) 
    emailMessage.setContent(richMessage.get, "text/html")
  else
	emailMessage.setText(message)

  emailMessage.setFrom(EmailAddress(from._1))
  to foreach (to_ => emailMessage addRecipient (Message.RecipientType.TO, EmailAddress(to_)))
  cc foreach (cc_ => emailMessage addRecipient (Message.RecipientType.CC, EmailAddress(cc_)))
  bcc foreach (bcc_ => emailMessage addRecipient (Message.RecipientType.BCC, EmailAddress(bcc_)))
}

object send {
  def a(mail: Mail) {
    Transport.send(mail.emailMessage)
  }
}