package com.aquila.actors

sealed trait Person
case object Man extends Person
case object Woman extends Person

import akka.actor.Actor
class Greeter extends Actor {
  def receive = {
    case Man => println("Hello...You are a man")
    case Woman => println("Hello...You are a woman")
  }
}